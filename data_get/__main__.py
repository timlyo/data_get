import argparse
from data_get import apis

parser = argparse.ArgumentParser()
subparsers = parser.add_subparsers(dest="api_name")


def main():
    for api in apis.api_list:
        api.setup_cmdline(subparsers)
    args = parser.parse_args()

    result = None
    for api in apis.api_list:
        if api.name == args.api_name:
            result = api.process_args(args)
            break
    print(result)


if __name__ == "__main__":
    main()
