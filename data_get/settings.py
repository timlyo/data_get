from tinydb import TinyDB, where, operations

import os
import errno


def make_sure_path_exists(path):
    try:
        os.makedirs(path)
    except OSError as exception:
        if exception.errno != errno.EEXIST:
            raise


class Settings:
    """ Store for user names/api keys et cetera """

    def __init__(self):
        home = os.path.expanduser("~")
        db_path = home + "/.data_get/"
        make_sure_path_exists(db_path)
        self.db = TinyDB(db_path + "/settings.json")

    def get_settings(self, name):
        return self.db.search(where("name") == name)

    def update(self, name, data):
        assert isinstance(data, dict)
        if not self.db.contains(where("name") == name):
            self.db.insert({"name": name})

        self.db.update(data, where("name") == name)


settings = Settings()
