from data_get.settings import settings
from .api import Api
import praw

"""
Wrapper around the Reddit api

Commands 
- subreddit
- user

"""

reddit = None


class Reddit(Api):
    def __init__(self):
        self.name = "reddit"

    def process_args(self, args):
        if args.action_name == "subreddit":
            return self.subreddit(args)
        elif args.action_name == "user":
            self.user(args)
        elif args.action_name == "settings":
            self.modify_settings(args)

    def get_settings(self) -> dict:
        reddit_settings = settings.get_settings("reddit")
        if len(reddit_settings) == 0:
            return None
        return reddit_settings[0]

    def setup_cmdline(self, subparsers):
        reddit_parser = subparsers.add_parser("reddit")
        reddit_subparser = reddit_parser.add_subparsers(dest="action_name")

        subreddit_parser = reddit_subparser.add_parser("subreddit")
        subreddit_parser.add_argument("name", help="Name of subreddit")
        subreddit_parser.add_argument("-limit", type=int, help="Number of results to return")
        subreddit_parser.add_argument("-page")
        subreddit_parser.add_argument("-download_images", action="store_true")

        user_parser = reddit_subparser.add_parser("user")
        user_parser.add_argument("username")

        settings_parser = reddit_subparser.add_parser("settings")
        settings_parser.add_argument("-client_id")
        settings_parser.add_argument("-client_secret")
        settings_parser.add_argument("-user_agent")
        settings_parser.add_argument("-username")
        settings_parser.add_argument("-password")
        settings_parser.add_argument("-list", action="store_true")

    def user(self, args):
        username = args.username
        print(username)
        reddit = self.get_client()
        print(reddit.redditor(username))

    def subreddit(self, args):
        print(args)
        name = args.name
        if args.page:
            assert args.page in ["hot", "top", "rising", "new"]

        reddit = self.get_client()
        subreddit = reddit.subreddit(name)
        page = None
        if args.page == "hot":
            page = subreddit.hot
        elif args.page == "top":
            page = subreddit.top
        elif args.page == "rising":
            page = subreddit.rising
        elif args.page == "new":
            page = subreddit.new
        else:
            print("Default", page)
            page = subreddit.hot

        submissions = []
        for submission in page(limit=args.limit):
            print(dir(submission))
            print()
            submissions.append({
                "title": submission.title,
                "author": submission.author,
                "score": submission.score,
                "created": submission.created,
                "id": submission.id,
                "num_comments": submission.num_comments,
                "over_18": submission.over_18,
                "subreddit": submission.subreddit.display_name,
                "url": submission.url,
            })
        if args.download_images:
            self.download_images(submissions)
        return submissions

    def download_images(self, submissions):
        for submission in submissions:
            print(submission["url"])

    def modify_settings(self, args):
        if args.client_id:
            settings.update("reddit", {"client_id": args.client_id})

        if args.client_secret:
            settings.update("reddit", {"client_secret": args.client_secret})

        if args.user_agent:
            settings.update("reddit", {"user_agent": args.user_agent})

        if args.username:
            settings.update("reddit", {"username": args.username})

        if args.password:
            settings.update("reddit", {"password": args.password})

        if args.list:
            reddit_settings = self.get_settings()
            if settings:
                print(reddit_settings)

    def get_client(self):
        reddit_settings = self.get_settings()
        if reddit_settings is None:
            # TODO make more informative
            print("Error no reddit settings found")
            return

        if reddit_settings.get("user_name") and reddit_settings.get("password"):
            client = praw.Reddit(client_id=reddit_settings["client_id"],
                                 client_secret=reddit_settings["client_secret"],
                                 user_agent=reddit_settings["user_agent"],
                                 username=reddit_settings["username"],
                                 password=reddit_settings["password"])
            assert not client.read_only
            return client

        return praw.Reddit(client_id=reddit_settings["client_id"],
                           client_secret=reddit_settings["client_secret"],
                           user_agent=reddit_settings["user_agent"])
