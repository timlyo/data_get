import os
from setuptools import setup, find_packages


def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()


setup(
    name="data_get",
    version="0.1.0",
    author="Tim Maddison",
    author_email="timlyomaddison@gmail.com",
    description=("A unified interface for multiple apis"),
    license="MIT",
    keywords="api",
    url="https://gitlab.com/timlyo/data_get",
    packages=find_packages(),
    long_description=read('readme.md'),
    entry_points={
        'console_scripts': [
            'data_get = data_get.__main__:main'
        ]
    },
)
